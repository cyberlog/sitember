# Work with Python 3.6
import discord
#print(discord.__version__)  # check to make sure at least once you're on the right version!

token = 'NjIwNzEwNzI1ODMyMTQ2OTY0.XXa0wg.oITHkGlXfbPRCNURTyymC_Y-Tec'

client = discord.Client()  # starts the discord client.


@client.event  # event decorator/wrapper. More on decorators here: https://pythonprogramming.net/decorators-intermediate-python-tutorial/
async def on_ready():  # method expected by client. This runs once when connected
    print(f'We have logged in as {client.user}')  # notification of login.


@client.event
async def on_message(message):  # event that happens per any message.

    # each message has a bunch of attributes. Here are a few.
    # check out more by print(dir(message)) for example.
    html = open("_layouts/sep10.html", "a")
    content = (f"{message.content}")
    html.write(content)
    html.close()
    if ('test') in message.content.lower():
     await message.channel.send('Say hello!')

client.run(token)  # recall my token was saved!